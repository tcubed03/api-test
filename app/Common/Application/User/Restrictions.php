<?php

namespace Fusion\Common\Application\User;

class Restrictions
{
    /**
     * @var array|string[]
     */
    private $countries;

    public function setCountries(array $countries): self
    {
        $this->countries = $countries;
        return $this;
    }

    public function getCountries(): array
    {
        return $this->countries ?? [];
    }
}
