<?php

declare(strict_types=1);

namespace Fusion\Common\Domain\Model;

/**
 * ValueObject
 *
 * @package Fusion\Common\Domain\Model
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 * @version 1.0.0
 */
abstract class ValueObject
{
    /**
     * Get a string representation of this value object when using string casting
     *
     * @return string
     */
    abstract public function __toString(): string;

    /**
     * Get a string representation of this value object
     *
     * @see ValueObject::__toString()
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->__toString();
    }

    /**
     * Compare another value object against this one to see if they are equal
     *
     * @param ValueObject|null $other
     *
     * @return bool
     */
    public function equals(?ValueObject $other): bool
    {
        // Compare the object type
        if (!$other instanceof static) {
            return false;
        }

        // Compare the property values
        $thisValues  = get_object_vars($this);
        $otherValues = get_object_vars($other);

        foreach ($otherValues as $propertyName => $otherValue) {
            // Use the appropriate comparator method for each property type
            switch (true) {
                case $otherValue instanceof \DateTimeInterface:
                    $thisValue = $thisValues[$propertyName];
                    $isEqual   = $thisValue instanceof \DateTimeInterface && $otherValue->getTimestamp() == $thisValue->getTimestamp();
                    break;

                case $otherValue instanceof \DateTimeZone:
                    $thisValue = $thisValues[$propertyName];
                    $isEqual   = $thisValue instanceof \DateTimeZone && $otherValue->getName() == $thisValue->getName();
                    break;

                case $otherValue instanceof ValueObject:
                    $isEqual = $otherValue->equals($thisValues[$propertyName]);
                    break;

                default:
                    $isEqual = $otherValue == $thisValues[$propertyName];
            }

            // If an inequality was found, return now
            if (!$isEqual) {
                return false;
            }
        }

        return true;
    }
}
