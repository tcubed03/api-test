<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

final class Circle extends ValueObject implements Shape
{
    /** @var Position */
    private $centre;
    /** @var Radius */
    private $radius;

    // Setup ----

    /**
     * Draw a circle with the given radius
     *
     * @param Position $centre
     * @param Radius   $radius
     *
     * @return Circle
     */
    public static function withRadius(Position $centre, Radius $radius): Circle
    {
        return new self($centre, $radius);
    }

    /**
     * Parse a string representation of a circle
     *
     * @param string $circleString
     *
     * @return Circle
     */
    public static function fromString(string $circleString): Circle
    {
        preg_match('/\(\[(-?\d+.\d+,-?\d+.\d+)\],(\d+.\d+)\)/', $circleString, $matches);
        Assert::that($matches)->notEmpty("The circle string is not in a valid format");

        $centre = Position::fromString($matches[1]);
        $radius = Radius::fromString($matches[2]);

        return new self($centre, $radius);
    }

    /**
     * Circle constructor.
     *
     * @param Position $centre
     * @param Radius   $radius
     */
    private function __construct(Position $centre, Radius $radius)
    {
        $this->centre = $centre;
        $this->radius = $radius;
    }

    // Commands ----

    /**
     * @inheritDoc
     */
    public function withAdjustedCoordinates(array $coordinates): Geometry
    {
        return new self(
            Position::fromCoordinates(
                (float) $coordinates[1],
                (float) $coordinates[0]
            ),
            Radius::fromFloat((float) $coordinates[2])
        );
    }

    // Queries ----

    /**
     * Get the centre position of this circle
     *
     * @return Position
     */
    public function centre(): Position
    {
        return $this->centre;
    }

    /**
     * Get this circle's radius
     *
     * @return Radius
     */
    public function radius(): Radius
    {
        return $this->radius;
    }

    /**
     *
     *
     * @return float
     */
    public function area(): float
    {
        $radius = $this->radius;
        $area   = M_PI * pow($radius->length(), 2);

        return (float) $area;
    }

    /**
     * @inheritDoc
     */
    public function centroid(): Position
    {
        return $this->centre;
    }

    /**
     * @inheritDoc
     */
    public function type(): string
    {
        return "Circle";
    }

    /**
     * @inheritDoc
     */
    public function coordinates(): array
    {
        return [
            $this->centre->coordinates(),
            $this->radius->length()
        ];
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf("Circle([%s],%s)", $this->centre, $this->radius);
    }
}
